package com.example.springsecurityrolepermissiontest.auth;

import com.example.springsecurityrolepermissiontest.model.MyUser;
import com.example.springsecurityrolepermissiontest.repository.MyUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ApplicationUserDetailService implements UserDetailsService {

    private final MyUserRepository myUserService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<MyUser> myUser = myUserService.findMyUserByUsername(username);
        if (myUser.isPresent()) {
            return new ApplicationUserDetails(myUser.get());
        }
        throw new UsernameNotFoundException("user detail not found");
    }
}
