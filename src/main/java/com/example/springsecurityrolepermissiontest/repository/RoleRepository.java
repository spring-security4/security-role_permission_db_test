package com.example.springsecurityrolepermissiontest.repository;

import com.example.springsecurityrolepermissiontest.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role,Integer> {
}
