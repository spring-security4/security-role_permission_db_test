package com.example.springsecurityrolepermissiontest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MyUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String username;
    private String password;
    @Column(unique = true)
    private String email;
    private String firstName;
    private String lastName;
    private LocalDateTime createdDate;

    @OneToOne
    @JoinColumn(name = "role_id")
    private Role role;

    @PrePersist
    private void createNow(){
        this.createdDate = LocalDateTime.now();
    }
}
