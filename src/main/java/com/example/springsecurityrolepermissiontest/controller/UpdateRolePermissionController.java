package com.example.springsecurityrolepermissiontest.controller;

import com.example.springsecurityrolepermissiontest.dto.UpdateRolePermissionRequest;
import com.example.springsecurityrolepermissiontest.service.MyUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UpdateRolePermissionController {

    private final MyUserService myUserService;

    @PutMapping("/give-access/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String updateRolePermission(@PathVariable Long id, @RequestBody UpdateRolePermissionRequest permissionRequest) {
        return myUserService.updateUserAccess(id, permissionRequest);
    }
}
