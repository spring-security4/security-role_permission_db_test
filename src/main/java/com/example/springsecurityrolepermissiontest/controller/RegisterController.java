package com.example.springsecurityrolepermissiontest.controller;

import com.example.springsecurityrolepermissiontest.dto.RegisterRequest;
import com.example.springsecurityrolepermissiontest.service.MyUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class RegisterController {

    private final MyUserService myUserService;

    @PostMapping("/register")
    public String register(@RequestBody RegisterRequest registerRequest) {
        return myUserService.createUser(registerRequest);
    }
}
