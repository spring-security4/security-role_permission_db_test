package com.example.springsecurityrolepermissiontest;

import com.example.springsecurityrolepermissiontest.auth.ApplicationUserDetailService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityRolePermissionTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityRolePermissionTestApplication.class, args);

    }

}
