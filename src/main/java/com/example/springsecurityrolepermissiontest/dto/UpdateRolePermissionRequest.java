package com.example.springsecurityrolepermissiontest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateRolePermissionRequest {
    private Integer roleId;
    private Integer[] permissions;
}
