package com.example.springsecurityrolepermissiontest.service;

import com.example.springsecurityrolepermissiontest.dto.RegisterRequest;
import com.example.springsecurityrolepermissiontest.dto.UpdateRolePermissionRequest;
import com.example.springsecurityrolepermissiontest.model.MyUser;
import com.example.springsecurityrolepermissiontest.model.Permission;
import com.example.springsecurityrolepermissiontest.model.Role;
import com.example.springsecurityrolepermissiontest.repository.MyUserRepository;
import com.example.springsecurityrolepermissiontest.repository.PermissioRepository;
import com.example.springsecurityrolepermissiontest.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MyUserService {

    private static final Integer ROLE_USER = 4;
    private static final Integer READ = 1;
    private final MyUserRepository myUserRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final PermissioRepository permissioRepository;

    public String createUser(RegisterRequest registerRequest) {
        MyUser myUser = new MyUser();
        myUser.setUsername(registerRequest.getUsername());
        myUser.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
        myUser.setEmail(registerRequest.getEmail());
        myUser.setFirstName(registerRequest.getFirstName());
        myUser.setLastName(registerRequest.getLastName());
        myUser.setRole(defaultRole());
        myUserRepository.save(myUser);
        return "SUCCESS";
    }

    public String updateUserAccess(Long id, UpdateRolePermissionRequest permissionRequest) {
        MyUser myUser = myUserRepository.getById(id);
        Role role = roleRepository.getById(permissionRequest.getRoleId());
        role.setPermissions(Arrays
                .stream(permissionRequest.getPermissions())
                .map(permissioRepository::getById)
                .collect(Collectors.toList()));
        myUser.setRole(role);
        myUserRepository.save(myUser);
        return "SUCCESS";
    }

    public Role defaultRole() {
        Optional<Role> role = roleRepository.findById(ROLE_USER);
        Permission permissions = permissioRepository.getById(READ);
        role.get().setPermissions(new ArrayList<>(Collections.singletonList(permissions)));
        return role.get();
    }
}
