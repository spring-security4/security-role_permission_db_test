package com.example.springsecurityrolepermissiontest.repository;

import com.example.springsecurityrolepermissiontest.model.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissioRepository extends JpaRepository<Permission,Integer> {
}
