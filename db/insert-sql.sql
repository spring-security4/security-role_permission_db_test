insert into public.my_user (created_date, email, first_name, last_name, password, username)
values  ('2021-06-10 11:01:37.000000', 'admin@gmail', 'Admin', 'admin', 'encode your password', 'admin123'),

insert into public.permission (id, permission_name)
values  ('TEST1-READ'),
        ( 'TEST1-WRITE'),
        ( 'TEST1-UPDATE'),
        ('TEST1-DELETE'),
        ('TEST2-READ'),
        ( 'TEST2-WRITE'),
        ('TEST2-UPDATE'),
        ( 'TEST2-DELETE'),
        ( 'TEST3-READ'),
        ( 'TEST3-WRITE'),
        ( 'TEST3-UPDATE'),
        ( 'TEST3-DELETE');


insert into public.role (id, role_name)
values  ( 'ADMIN'),
        ( 'SUPERVISOR'),
        ( 'MANAGER'),
        ( 'USER');

insert into public.role_permissions (role_id, permission_id)
values  (1, 1),
        (1, 2),
        (1, 3),
        (1, 4),
        (1, 5),
        (1, 6),
        (1, 7),
        (1, 8),
        (1, 9),
        (1, 10),
        (1, 11),
        (1, 12),
        (2, 1),
        (2, 2),
        (2, 3),
        (2, 4),
        (2, 5),
        (2, 6),
        (2, 7),
        (2, 8),
        (2, 9),
        (2, 10),
        (3, 1),
        (3, 2),
        (3, 3),
        (3, 4),
        (3, 5),
        (3, 6),
        (3, 7),
        (4, 1);